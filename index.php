<?php
//Подключаем классы
include(__DIR__.'/aiv_dev/FbAccountKit.php');
include(__DIR__.'/aiv_dev/GraphAccountKit.php');

//Параметры подключения к приложению FbAccountKit
$appId = '891348694355983';
$secret = '905be82351db7c4143c2b47cd94e09da';

//Стартуем сессию и пишем туда параметры FbAccountKit, если они есть в запросе
\aiv_dev\GraphAccountKit::startSession();

//Если пользователь авторизироваан, формируем информацию о нем
if (\aiv_dev\FbAccountKit::isSessionStarted()) {
    $final = \aiv_dev\GraphAccountKit::getAccountInformation(
        $appId,
        $secret
    );
}
//Если пользователь не авторизирован, инициализируем скрипт для авторизации
$fbak = new \aiv_dev\FbAccountKit(
    [
        'appId' => $appId,
        'secret' => $secret,
    ]
);


if (!$fbak->verifying) {
//Кнопки для авторизации
    echo $fbak->getButton('By phone', ['tag' => 'button'], 'phone').'&nbsp;';
    echo $fbak->getButton('By email', ['tag' => 'button'], 'email');
} else {

    $final = (array) $final;

//Вывод информации о пользователе
    ?>
    <strong>ID:</strong> <?= $final['id'] ?> <br>
    <?php
    if (isset($final['email'])) {
        ?>
        <strong>Email:</strong> <?= $final['email']->address ?>
        <br>
        <br>
        <?php
    }
    if(isset($final['phone'])) {
        ?>
        <strong>Phone
            Number:</strong> <?= $final['phone']->number ?>
        <br>
        <br>
        <?php
    }
//Разлогинивание
    echo $fbak->getButton('Logout', ['tag' => 'button'], 'logout');
}

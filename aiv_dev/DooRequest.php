<?php
/**
 * Created by PhpStorm.
 * User: AIvDeV
 * Date: 26.09.2017
 * Time: 20:41
 *
 * DooRequest Класс для построения, отправки и обработки cUrl GET запросов
 */

namespace aiv_dev;


class DooRequest
{


    /**
     * Отправка cUrl GET запроса с заданными параметрами
     *
     * @param string $url
     * @param array $params параметры запроса
     * @return array результаты запроса.
     */
    public static function getRequest($url = null, $params = [])
    {

        if (empty($url)) {
            return false;
        }

        $strParams = self::getParamString($params);
        $url = trim("$url?$strParams");

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        if(curl_errno($ch)){
            throw new \Exception(curl_error($ch));
        }
        curl_close($ch);
        $info = json_decode($result);

        return $info;
    }

    /**
     * Построение строки парааметров запроса
     *
     * @param array $params параметры запроса
     * @return string строка параметров.
     */
    public static function getParamString(array $params = [])
    {
        return http_build_query($params)."\n";
    }

}


<?php
/**
 * Created by PhpStorm.
 * User: AIvDeV
 * Date: 26.09.2017
 * Time: 20:39
 *
 * GraphAccountKit класс для работы с одноименным API для получения access_token и последующей авторизации
 *
 */

namespace aiv_dev;


class GraphAccountKit
{
    //URL запроса для получения access_token.
    const GET_TOKEN_URL = 'https://graph.accountkit.com/v1.0/access_token/';
    //URL запроса для данных пользователя.
    const GET_INFO_URL = 'https://graph.accountkit.com/v1.0/me/';

    /**
     * Метод получения данных пользователя
     *
     * @param string $appId номер приложения
     * @param string $secret секретная строка для приложения
     * @return array|object Данные пльзователя.
     */
    public static function getAccountInformation($appId = null, $secret = null)
    {
        $infoBySession = self::getInfoBySession();

        if(!empty($infoBySession)){
            return $infoBySession;
        }

        if (empty($_SESSION['FbAccountKit']['fbak_code'])){
            return false;
        }

        if (empty($appId) || empty($secret)) {
            throw new \Exception('No set params for request!');
        }
        $tokenResponse = self::getAccessToken($appId, $secret);

        if ($tokenResponse->error) {
            throw new \Exception(
                $tokenResponse->error->message
            );
        }

        $url = self::GET_INFO_URL;

        $params = [
            'access_token' => $tokenResponse->access_token,
        ];

        $info = DooRequest::getRequest($url, $params);

        if ($info->error) {
            throw new \Exception($info->error->message, $info->error->code);
        }
        unset($_SESSION['FbAccountKit']);
        self::saveInfo($info);

        return $info;


    }

    /**
     * Метод получения access_token
     *
     * @param string $appId номер приложения
     * @param string $secret секретная строка для приложения
     * @return object access_token.
     */
    private static function getAccessToken($appId, $secret)
    {
        $url = self::GET_TOKEN_URL;

        $params = [
            'grant_type' => 'authorization_code',
            'code' => $_SESSION['FbAccountKit']['fbak_code'],
            'access_token' => self::getToken($appId, $secret),
        ];

        include(__DIR__.'/DooRequest.php');

        return DooRequest::getRequest($url, $params);
    }

    /**
     * Метод генерации изначального token
     *
     * @param string $appId номер приложения
     * @param string $secret секретная строка для приложения
     * @return boolean|object token.
     */
    private static function getToken($appId, $secret)
    {
        if (!empty($appId) && !empty($secret)) {
            return $token = 'AA|'.$appId.'|'.$secret;
        }

        return false;
    }

    /**
     * Запуск сессии и запись fbak_code и fbak_state в нее.
     *
     * @return boolean
     */
    public static function startSession()
    {
        session_start();
        $request = $_REQUEST ? $_REQUEST : [];
        if (!empty($request['fbak_code']) && !empty($request['fbak_state']) && empty($_SESSION['FbAccountKit']['id'])) {
            unset($_SESSION['FbAccountKit']);
            self::saveInfo($request);
            return true;
        }

        return false;
    }

    /**
     * Метод для записи в сессию указанных параметров
     *
     * @param array $info параметры для записи
     * @return boolean
     */
    private static function saveInfo($info = []){
        foreach ($info as $key=>$value){
            $_SESSION['FbAccountKit'][$key]=$value;
        }
        return true;
    }

    /**
     * Получение данных о пользователе из сессии
     *
     * @return array|boolean
     */
    private static function getInfoBySession(){
        if(!empty($_SESSION['FbAccountKit']['id'])){
            return $_SESSION['FbAccountKit'];
        }else{
            return false;
        }
    }

}
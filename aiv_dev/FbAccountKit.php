<?php
/**
 * Created by PhpStorm.
 * User: AIvDeV
 * Date: 25.09.2017
 * Time: 12:08
 *
 * FbAccountKit класс для работы с FbAccountKit, его инициализации и авторизации
 */

namespace aiv_dev;


class FbAccountKit
{

    // Действи при нажатии кнопки в зависимости от параметра 'action'
    protected static $ACTION_BUTTON
        = [
            'phone' => 'phone_btn_onclick()',
            'email' => 'email_btn_onclick()',
            'logout' => ' document.location = "logout.php"; ',
        ];

    //Метод отправки данных формы авторизации
    public $requestMethod = 'POST';
    //Экшн формы авторизации
    public $requestAction = '';
    //Индикатор авторизации пользователя
    public $verifying = false;
    //Данные пользователя
    public $accountInformation;
    //Ид формы авторизации
    protected $requestIdForm = 'fbak_form';
    //Параметры инициализации FbAccountKit:
    protected $appId;
    protected $secret;
    protected $version = 'v1.0';
    protected $debug = false;
    protected $fbAppEventsEnabled = true;
    protected $state;

    public function __construct(array $params = [])
    {
        if (self::isSessionStarted()
            && !empty($_SESSION['FbAccountKit']['id'])
        ) {
            $this->verifying = true;
        } else {
            $this->initAccountKit($params);
        }
    }


    /**
     * Индикатор запущеной сессии
     *
     * @return boolean.
     */
    public static function isSessionStarted()
    {
        if (php_sapi_name() !== 'cli') {
            if (version_compare(phpversion(), '5.4.0', '>=')) {
                return session_status() === PHP_SESSION_ACTIVE ? true : false;
            } else {
                return session_id() === '' ? false : true;
            }
        }

        return false;
    }


    /**
     * Инициализация FbAccountKit
     *
     * @param array $params параметры инициализации
     */
    public function initAccountKit($params)
    {
        if ($this->setParams($params)):
            ?>
            <script type="text/javascript"
                    src="https://sdk.accountkit.com/ru_RU/sdk.js"></script>

            <script type="text/javascript">
                // initialize Account Kit with CSRF protection
                AccountKit_OnInteractive = function () {
                    AccountKit.init(
                        {
                            appId: "<?=$this->appId?>",
                            state: "<?=random_int(100000000, 999999999)?>",
                            version: "<?=$this->version?>",
                            debug: "<?=$this->debug?>",
                            fbAppEventsEnabled: "<?=$this->fbAppEventsEnabled?>",
                        }
                        //If your Account Kit configuration requires app_secret, you have to include ir above
                    );
                };
                // login callback
                function fbLoginCallback(response) {
                    console.log(response);
                    if (response.status === "PARTIALLY_AUTHENTICATED") {
                        var form = document.getElementById("<?=$this->requestIdForm?>");
                        form.insertAdjacentHTML('beforeend', '<input type="hidden" name="fbak_code" id="fbak_code" value="' + response.code + '">');
                        form.insertAdjacentHTML('beforeend', '<input type="hidden" name="fbak_state" id="fbak_state" value="' + response.state + '">');
                        form.submit();
                    }
                    else if (response.status === "NOT_AUTHENTICATED") {
                        // handle authentication failure
                        console.log("Authentication failure");
                    }
                    else if (response.status === "BAD_PARAMS") {
                        // handle bad parameters
                        console.log("Bad parameters");
                    }
                }
                // phone form submission handler
                function phone_btn_onclick() {
                    // you can add countryCode and phoneNumber to set values
                    AccountKit.login('PHONE', {}, // will use default values if this is not specified
                        fbLoginCallback);
                }
                // email form submission handler
                function email_btn_onclick() {
                    // you can add emailAddress to set value
                    AccountKit.login('EMAIL', {}, fbLoginCallback);
                }
                // destroying session
                function logout() {
                    document.location = 'logout.php';
                }
            </script>

            <?php
            if ($this->requestIdForm == 'fbak_form'):
                ?>
                <form action="<?= $this->requestAction ?>"
                      method="<?= $this->requestMethod ?>" id="fbak_form">
                </form>

                <?php
            endif;
        endif;

    }


    /**
     * Метод загрузки параметров для инициализации FbAccountKit
     *
     * @param array $params параметры запроса
     * @return boolean.
     */
    public function setParams(array $params = [])
    {
        if (empty($params['secret']) || empty($params['appId'])) {
            return false;
        }
        $this->appId = $params['appId'];
        $this->secret = $params['secret'];
        $this->version = !empty($params['version']) ? $params['version']
            : $this->version;
        $this->debug = !empty($params['debug']) ? $params['debug']
            : $this->debug;
        $this->state = !empty($params['state'])
            ? $params['state']
            : random_int(
                100000000,
                999999999
            );
        $this->fbAppEventsEnabled = !empty($params['fbAppEventsEnabled'])
            ? $params['fbAppEventsEnabled'] : $this->fbAppEventsEnabled;

        return true;
    }

    /**
     * Построениие функциональных кнопок
     *
     * @param string $name название кнопки
     * @param array $params параметры построения
     * @param string $action вид действия кнопки
     * @return string.
     */
    public function getButton($name = 'btn', array $params = [], $action = 'phone')
    {
        $type = $params['type'] ? $params['type'] : 'button';
        $class = $params['class'] ? $params['class'] : 'btn btn-default';
        $style = $params['style'] ? implode('; ', $params['style']) : '';
        $id = $params['id'] ? $params['id'] : '';
        $href = $params['href'] ? $params['href'] : '';
        $tag = $params['tag']
        && in_array(
            $params['tag'],
            ['a', 'button', 'input']
        ) ? $params['tag'] : 'a';
        $onclick = $params['onclick'] ? $params['onclick'] : '';
        $onclick = !empty($onclick) ? $onclick : self::$ACTION_BUTTON[$action];

        return "<$tag type='$type' class='$class' style='$style' id='$id' href='$href' onclick='$onclick'>$name</$tag>";
    }

}